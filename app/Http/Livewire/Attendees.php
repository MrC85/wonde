<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Support\Collection;
use Livewire\Component;

/**
 *
 */
class Attendees extends Component
{
    /**
     * @var string
     */
    public string $teacher = '';

    /**
     * @var string
     */
    public string $class = '';

    /**
     * @return void
     */
    public function resetClass(): void
    {
        $this->class = '';
    }

    /**
     * @param \Wonde\Client $wonde
     * @return Collection
     */
    public function getEmployeesProperty(\Wonde\Client $wonde): Collection
    {
        //currently returns all the teachers for the school but can be locked down on a privilege basis
        // i.e. Head Teacher can see all, individual teachers can only see themselves
        return cache()->remember('employees', 300, function () use ($wonde) {
            return collect($wonde
                ->school(config('wonde.default_school'))
                ->employees
                ->all(['classes', 'employment_details']))
                ->filter(function(\stdClass $employee) {
                    return count($employee->classes->data) > 0 && $employee->employment_details->data->current === true;
                });
        });
    }

    /**
     * @param \Wonde\Client $wonde
     * @return Collection
     */
    public function getClassesProperty(\Wonde\Client $wonde): Collection
    {
        if($this->teacher !== '') {
            return collect($this->employees->where('id', $this->teacher)->first()->classes->data);
        }

        return collect();
    }

    /**
     * @param \Wonde\Client $wonde
     * @return Collection
     */
    public function getLessonsProperty(\Wonde\Client $wonde): Collection
    {
        if($this->class !== '') {
            $class = $wonde->school(config('wonde.default_school'))->classes->get($this->class, ['students', 'lessons']);

            return collect([
                'students' => $class->students->data,
                'lessons' => collect($class->lessons->data)->groupBy(function(\stdClass $lesson) {
                return Carbon::parse($lesson->start_at->date)->format('Y-m-d');
            })]);
        }

        return collect();
    }

    /**
     * @return View|Application|Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function render(): View|Application|Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('livewire.attendees');
    }
}
