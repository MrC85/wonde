<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Wonde\Client;

class WondeServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(Client::class, function ($app) {
            return new Client(config('wonde.token'));
        });
    }

    public function boot(): void
    {
    }
}
