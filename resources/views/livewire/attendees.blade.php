<div x-data="{ teacher: @entangle('teacher'), class: @entangle('class') }" class="w-1/2 mx-auto flex-row items-center justify-center">
    <div class="flex w-full justify-around items-center">
        <div class="form-control w-full max-w-xs">
            <label class="label">
                <span class="label-text">Teacher</span>
            </label>
            <select wire:model="teacher" class="select select-bordered w-full max-w-xs" wire:change="resetClass">
                <option disabled selected value="">Select a teacher...</option>
                @foreach($this->employees as $employee)
                    <option value="{{ $employee->id }}">{{ $employee->title . ' ' . $employee->forename . ' ' .$employee->surname }}</option>
                @endforeach
            </select>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
            <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75" />
        </svg>

        <div class="form-control w-full max-w-xs">
            <label class="label">
                <span class="label-text">Class</span>
            </label>
            <select wire:model="class" class="select select-bordered w-full max-w-xs" x-bind:disabled="teacher == ''">
                <option disabled selected value="">Select a class...</option>
                @foreach($this->classes as $class)
                    <option value="{{ $class->id }}">{{ $class->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    @if($this->lessons->isNotEmpty())
        <div class="w-full mt-4 flex flex-wrap">
            @foreach($this->lessons['lessons'] as $date => $lesson)
                <div class="flex-col p-2 bg-gray-800 w-1/3 mb-2">
                    <h3 class="text-lg">{{ \Carbon\Carbon::parse($date)->format('l jS F, Y') }}</h3>
                    <ul class="ml-2 mt-2">
                        @foreach($this->lessons['students'] as $student)
                            <li>{{ $student->forename }} {{ $student->surname }}</li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
        </div>
    @endif
</div>
