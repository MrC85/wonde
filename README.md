<h1>Wonde Technical Exercise</h1>

<h5>Built using</h5>
<ul>
<li>PHP 8.1</li>
<li>Laravel 10</li>
<li>Livewire / Alpine JS</li>
<li>MariaDB 10.16 (Compatible with MySQL)</li>
<li>Redis</li>
<li>Daisy UI</li>
</ul>

To install
<ul>
<li>composer update --optimize-autoloader</li>
<li>create .env etc</li>
<li>php artisan migrate</li>
<li>npm install</li>
<li>npm run dev</li>
</ul>

<h5>The app is protected by Laravel Sanctum</h5>
A test user has been created with the following authentication credentials:
U: peter@wonde.test
P: password

You can also create a new user by navigating to /register
