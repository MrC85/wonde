<?php

namespace Tests\Feature\Livewire;

use App\Http\Livewire\Attendees;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class AttendeesTest extends TestCase
{
    /** @test */
    public function the_component_can_render()
    {
        $component = Livewire::test(Attendees::class);

        $component->assertStatus(200);
    }
}
